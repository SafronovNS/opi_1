#include <iostream>
#include <iomanip>
#include <ctime>

using namespace std;

// ���������

//	* �������� ������� �� ���������

const int DEFAULT_SIDE = 17;

enum DEFAULT_MATRIX_BORDERS 
{
	DEFAULT_LEFT_BORDER = -10,
	DEFAULT_RIGHT_BORDER = 10
};

const int MENU_TASK_COUNT = 4;

enum MENU_POINT 
{
	FIND_MAX = 1,
	EQUALITY,
	FIND_MIN,
	CHANGE_MATRIX
};

const char MENU_TASK_NAMES[MENU_TASK_COUNT][61]
{
	"����� �������� � �������� � ������� ��������",
	"��������� ��������� ����������",
	"����������� ������� ����� ��������� ���������������� �������",
	"�������� �������"
};

int** createRandomSquareMatrix(int arraySide = DEFAULT_SIDE, int leftBorder = DEFAULT_LEFT_BORDER, int rightBorder = DEFAULT_RIGHT_BORDER) 
{
  int** output = new int* [arraySide];

  for (int i{}; i < arraySide; i++) 
  {
    output[i] = new int[arraySide];
    for (int j{}; j < arraySide; j++) { output[i][j] = leftBorder + rand() % (rightBorder - leftBorder + 1); }
  }

  return output;
}

void printSquareMatrix(int** array, int side) 
{
	for (int i{}; i < side; i++) 
	{
		for (int j{}; j < side; j++) { cout << setw(5) << array[i][j]; }
		cout << endl;
	}
}

int getEvenColumnMaxElement(int** array, int side, int minValue = DEFAULT_LEFT_BORDER) 
{
  int output{ minValue - 1 };

  for (int i{}; i < side; i++) 
  {
    for (int j{}; j < side; j += 2) 
    {
      if (array[i][j] >= output) { output = array[i][j]; }
    }
  }

  return output;
}

bool areDiagonalsEqual(int** array, int arraySide) 
{
	bool output{ };

	for (int i{}; (i < arraySide) && !output; i++)
	{
		if (array[i][i] != array[i][arraySide - i - 1]) { output = true; }
	}

	return !output;
}

int getMinElementBelowDiagonal(int** array, int arraySide, int max = DEFAULT_RIGHT_BORDER)
{
  int output{ max + 1 };

  for (int i{}; i < arraySide; i++)
  {
    for (int j{}; j <= i; j++) 
    {
      if (array[i][j] < output) {  output = array[i][j]; }
    }
  }

  return output;
}

void getMatrixUserInput(int& arraySide, int& leftBorder, int& rightBorder) 
{
	arraySide = -1;
	while (arraySide <= 0) 
	{
		cout << "������� ������� ���������� �������: ";
		cin >> arraySide;
	}

	cout << "������� ����� ������� ��������� �����: ";
	cin >> leftBorder;

	rightBorder = leftBorder - 1;
	while (rightBorder < leftBorder) 
	{
		cout << "������� ������ ������� ��������� �����: ";
		cin >> rightBorder;
	}
}

int main() 
{
	setlocale(LC_ALL, "Russian");

	srand(int(time(0)));

	int arraySide{ DEFAULT_SIDE }, leftBorder{ DEFAULT_LEFT_BORDER }, rightBorder{ DEFAULT_RIGHT_BORDER };

	int** array = createRandomSquareMatrix(arraySide, leftBorder, rightBorder);

	bool loop{ true };

	while (loop) 
	{
		cout << "������� ������� " << arraySide << 'x' << arraySide << " �� ��������� ����� ������� [" << leftBorder << "; " << rightBorder << "]:" << endl;
		printSquareMatrix(array, arraySide);

		cout << endl;
		for (int i{}; i < MENU_TASK_COUNT; i++) { cout << i + 1 << ". " << MENU_TASK_NAMES[i] << endl; }
		cout << "��� ������ ������� ����� ������ ������." << endl << endl << "�������� ����� ����: ";

		int pointInput, max{}, min{};
		cin >> pointInput;

		system("cls");

		switch (pointInput)
		{

		case MENU_POINT::FIND_MAX:
			max = getEvenColumnMaxElement(array, arraySide, leftBorder);

			if (max != leftBorder - 1) { cout << "�������� � �������� ������� � ������� ��������: " << max << endl; }
			else { cout << "�������� �� ������." << endl; }

			break;

		case MENU_POINT::EQUALITY:
			if (areDiagonalsEqual(array, arraySide)) { cout << "��������� �����." << endl; }
			else { cout << "��������� �� �����." << endl; }

			break;

		case MENU_POINT::FIND_MIN:
			min = getMinElementBelowDiagonal(array, arraySide, rightBorder);

			if (min != rightBorder + 1) { cout << "����������� ������� ����� ��������� ���������������� �������: " << min << endl; }
			else { cout << "������� �� ������." << endl; }

			break;

		case MENU_POINT::CHANGE_MATRIX:
			for (int i{}; i < arraySide; i++) {	delete[] array[i]; }
			delete[] array;

			getMatrixUserInput(arraySide, leftBorder, rightBorder);
			array = createRandomSquareMatrix(arraySide, leftBorder, rightBorder);

			break;

		default:
			loop = false;

			break;

		}
		cout << endl;
	}
	return 0;
}